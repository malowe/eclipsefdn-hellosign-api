SHELL = /bin/bash
pre-install:;
	mkdir -p ./config/mariadb/initdb.d
	[ -f ./config/mariadb/initdb.d/sample.sql ] && echo "EF DB dump already exists, skipping fetch" || (scp remote-host-name:~/webdev/sql/sample.sql.gz ./config/mariadb/initdb.d/ && gzip -d ./config/mariadb/initdb.d/sample.sql.gz )
dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties
clean:;
	mvn clean
install-yarn:;
	yarn install --frozen-lockfile --audit
generate-spec: validate-spec;
	yarn run generate-json-schema
validate-spec: install-yarn;
compile-java: generate-spec;
	mvn compile package
compile-java-quick: generate-spec;
	mvn compile package -Dmaven.test.skip=true
compile: clean compile-java;
compile-quick: clean compile-java-quick;
compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d
