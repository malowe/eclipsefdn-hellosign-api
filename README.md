# eclipsefdn-sample-api

Description of business need fulfilled by API goes here.

[[_TOC_]]

## Application details

|Service name|Internal port|Host port|
|-|-|-|
|Application|8090|10300|
|MariaDB|3306|10301|
|FoundationDB API|8095|10302|

## How to use

### Creating a new project

The following section will denote the customizations to make in order to customize this project from its raw template form to something useful for the exact project that it is being applied to.

#### POM

1. Update POM artifact name
1. Update the Common API version and Quarkus version to latest
1. If persistence isn't going to be used, remove the following dependencies:  
    - org.eclipsefoundation#quarkus-persistence  
    - io.quarkus#quarkus-devservices-h2  
    - io.quarkus#quarkus-jdbc-h2  
    - io.quarkus#quarkus-flyway  
    - com.h2database#h2  
1. If entity-to-entity mapping isn't required, then the following operations should be done:  
    1. Remove the mapstruct property from the top of the file  
    1. Remove the following dependencies:  
        - org.mapstruct#mapstruct  
        - org.mapstruct#mapstruct-processor  
    1. In the build `maven-compiler-plugin`, remove the `path` object that references mapstruct  


#### Makefile

1. Depending on if the application uses persistence, please choose one of the following:
    1. If the application does not use persistence, remove the pre-install phase. This is currently used to pull SQL files from remote before starting services.
    1. If the application does use persistence, update the pre-install phase, then update the command to pull SQL from the remote to fetch the proper SQL dumps. Below is the explanation of the current command and logic:
        - `[ -f ./config/mariadb/initdb.d/<upstream file name>.sql ] && echo "EF DB dump already exists, skipping fetch" ||` checks if the SQL file has been already retrieved. If so, the console will print out that the fetch is skipped and end early.
        - `scp remote-host-name:~/webdev/sql/sample.sql.gz ./config/mariadb/initdb.d/` fetches a file from the remote server, retrieving a GZIP'd file and placing it in the mariadb initialization script folder
        - `&& gzip -d ./config/mariadb/initdb.d/eclipsefoundation.sql.gz` uncompresses the file so long as the copy succeeded. This will uncompress it in place and replace the GZIP'd file, reducing burden on cleanup.
    1. Should there need to be new files sent to the remote server for secure storage, be sure to compress them using GZIP to save space and network traffic from the secure cluster.
1. Ensure that the makefile still properly uses tabs over spaces, as Makefiles do not function properly with spaces.

#### Jenkinsfile

1. Update references to `eclipsefdn-sample-api` to reference new project.
1. When the API is available on OKD, uncomment the `Deploy to cluster` step. This has been disabled to stop build failures and unnecessary operations on the cluster.

#### Docker-compose.yaml

1. If the project does not need persistence or the FoundationDB API, remove the 'mariadb' service from the file.
1. If the project does not need to bind to FoundationDB API, remove the 'foundationdb' service from the file. 

#### OKD

1. Remove all instances of `eclipsefdn-sample-api` from the `staging.yaml` and `production.yaml` under the `./src/main/k8s` folder.
1. For the final section of each yaml config demarcated by `---`, update the `spec.path` to reflect the publicly available URL for this API, taking care not to use generic paths that collide with other routes. IF the route cannot be properly defined yet, remove the final section of this file.

#### Miscellaneous

1. Register ports in sequential order in the Toolkit wiki "[Useful links](https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-webdev-toolkit/-/wikis/Documentation/A-Starting-Place/Useful-links)" page under Java services.
1. Create the Jenkins job under the Webdev CI instance.
1. Add the webhook in Gitlab linking it to Gitlab, using the following properties:
    - URL: https://foundation.eclipse.org/ci/webdev/gitlab-webhook/post
    - Secret: Retrieve current value from Bitwarden under the `Gitlab Webhook Secret` secure note.
    - Triggers: Check the "Merge Request events" as the only trigger
    - Additionally, enable SSL verification
1. Update the CONTRIBUTING and NOTICE files, replacing `eclipsefdn-sample-api` text with the name of the current project.

#### README

1. Remove the `## How to use` section along with any sub-sections associated to it.
1. Replace references to `eclipsefdn-sample-api` with the name of the project that is being used.
1. Update or remove the top level description under the first level heading.
1. Update the Application Details table to reflect the ports used in the application for ease of use.
1. Add details to the `Initial Setup` section.

## Getting started

### Requirements

* Docker
* mvn
* make
* yarn
* Java 11 >

### Setup

Additional setup instructions will be added as the project grows and evolves.

#### Initial setup

// TODO - depends on project, varies too widely to be easily templated

#### Build and start server

```bash
make compile-start
```

#### Live coding dev mode

```bash
make dev-start
```

#### Generate spec

```bash
make generate-spec
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-sample-api](https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-sample-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-sample-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
